package edu.iu.tableau;

/**
 * Created by vibhatha on 11/18/16.
 */

import tabRest.tabRest;

public class TabTester {
    // define these static vars for testing
    static String ts_url = "http://ts1.datapainters.com";
    static String ts_user = "admin";
    static String ts_pass = "admin";
    static String user_name = "admin";
    static String ts_site = ""; // this must be the contextUrl for the site, not the name.

    public static void main(String[] args) {

        // turn this on if you use fiddler
        System.setProperty("http.proxyHost", "127.0.0.1");
        System.setProperty("http.proxyPort", "8888");

        // This is the
        String workbook_name = "TestBook3";
        String view_name = "Sheet 3";
        String project_name = "default";
        String favorite_name = "My Workbook Fav1";
        String tag_name = "My";
        String datasource_location = "/home/vibhatha/Documents/IU /Semester 1 /Big Data Analytics/Tableau/tableau/rest/java/TestBook1.xlsx";
        String datasource_save_location = "/home/vibhatha/Documents/IU /Semester 1 /Big Data Analytics/Tableau/tableau/rest/java/ResultBook1.xlsx";
        String workbook_location = "/home/vibhatha/Documents/IU /Semester 1 /Big Data Analytics/Tableau/tableau/rest/java/TestBook1.xlsx";
        String save_location = "/home/vibhatha/Documents/IU /Semester 1 /Big Data Analytics/Tableau/tableau/rest/java/TestBook1_new.xlsx";

        ////////////////////////////////////////////
        // WORKBOOKS
        ////////////////////////////////////////////

        // testWorkbooks
        //testWorkbooks(workbook_location, workbook_name, project_name, save_location, favorite_name, tag_name);

        ////////////////////////////////////////////
        // PROJECTS
        ////////////////////////////////////////////

        // testProjects
        //testProjects();

        ////////////////////////////////////////////
        // DATASOURCES
        ////////////////////////////////////////////

        // testDataSources
        testDataSources(datasource_location,datasource_save_location);

        ////////////////////////////////////////////
        // GROUPS AND USERS
        ////////////////////////////////////////////

        // testAll users and groups functions
        //testUsersGroups();

        ////////////////////////////////////////////
        // SITES
        ////////////////////////////////////////////

        // test all the site functions
        // testSites();

        ////////////////////////////////////////////
        // EXTRAS
        ////////////////////////////////////////////

        // test adding users and groups
        //testAddGroupsUsers();

        // test adding users and groups
        //testDeleteGroupsUsers();

        // test getting users from site
        //testGetGroupsFromSite();

        // test getting users from site
        //testGetUsersFromSite();

        // test getting users from group
        //testGetUsersFromGroup();

        // test downloading a workbook
        //testPublishWorkbook(workbook_location, workbook_name, project_name);

        //testWorkbookAddFav(workbook_name, "Favorite1");
        //testWorkbookAddTag(workbook_name, "Tag1");
        //testAddViewFav(workbook_name, view_name, "View Fav 1");

        // test publishing a workbook
        //testDownloadWorkbook(workbook_name, save_location);

        // test things from a workbook
        //testDeleteWorkbookFav(workbook_name);
// API: ERROR		testDeleteWorkbookTag(workbook_name, "Tag1");
        //testDeleteViewFav(workbook_name, view_name);
        //testDeleteWorkbook(workbook_name);

        // test getting workbook permissions
        //testGetWorkbookPermissions();

        // test getting all of the workbooks + views + connections
        //testGetAllWorkbooks();

        // test updating workbook
        //testUpdateWorkbook(workbook_name, project_name);

    }

    public static void testSites() {
        String[] users = null;
        String respense = "";

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        String site_id = "";

        System.out.println("START Add Site ###");
        try {
            // now publish the workbook
            site_id = tab1.tsAddSite("Wes Test Site 1");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Site Already Exists");
            //e.printStackTrace();
        }
        try {
            site_id = tab1.tsGetSiteIdByName("Wes Test Site 1");
        } catch (Exception e2) {
            //
            System.out.println("--Something is wrong");
            site_id = "";
        }
        System.out.println("DONE Add Site: " + site_id);
        System.out.println("START Update Site ###");
        try {
            // now publish the workbook
            String msite_name = tab1.tsGetSiteNameById(site_id);
            String msite_url = tab1.tsGetSiteContentUrlByName(msite_name);
            tabRest tab2 = new tabRest(ts_url, ts_user, ts_pass, msite_url);
            respense = tab2.tsUpdateSite(site_id, "Wes Test Site 33","","","","","","");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Site Update failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Update Site: " + site_id);
        try {
            Thread.sleep(3000);                 //1000 milliseconds is one second.
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println("START Check Sites ###");
        try {
            users = tab1.tsGetSites();
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = users.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(users[mycnt]!=null){
                // parse the separate fields
                String msite_id = users[mycnt].split(",")[0];
                String site_name = users[mycnt].split(",")[1];
                String site_curl = users[mycnt].split(",")[2];

                // display the results
                System.out.println("SiteName: " + site_name);
                try {
                    System.out.println("toID: " + tab1.tsGetSiteIdByName(site_name));
                    System.out.println("toName: " + tab1.tsGetSiteNameById(msite_id));
                    System.out.println("ContentUrl: " + tab1.tsGetSiteContentUrlByName(site_name));
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }
                System.out.println("---");


            }
            mycnt++;
        }
        System.out.println("DONE Check Sites: ");

        System.out.println("START Delete Site ###");
        try {
            // now publish the workbook
            System.out.println("ID:"+site_id);
            String msite_name = tab1.tsGetSiteNameById(site_id);
            System.out.println("NAME:"+msite_name);
            String msite_url = tab1.tsGetSiteContentUrlByName(msite_name);
            System.out.println("URL:"+msite_url);
            tabRest tab2 = new tabRest(ts_url, ts_user, ts_pass, msite_url);
            tab2.tsDeleteSite(site_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Site Delete Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Delete Site: ");


    }

    public static void testDataSources(String datasource_location, String datasource_save_location) {
        String[] users = null;
        String respense = "";

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        String datasource_id = "";

        System.out.println("START Publish DataSource ###");
        try {
            // now publish the workbook
            datasource_id = tab1.tsPublishDataSource(datasource_location, "Wes Test DataSource 1", "default");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--DataSource Already Exists");
            //e.printStackTrace();
        }
        if(datasource_id.length()<1)
        {
            try {
                datasource_id = tab1.tsGetDataSourceIdByName("Wes Test DataSource 1");
            } catch (Exception e2) {
                //
                System.out.println("--Something is wrong");
                datasource_id = "";
            }
        }
        System.out.println("DONE Add DataSource: " + datasource_id);
        System.out.println("START Download DataSource ###");
        try {
            // now publish the workbook
            String ds_name = tab1.tsGetDataSourceNameById(datasource_id);
            respense = tab1.tsDownloadDataSource(ds_name, datasource_save_location);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--DataSource Download failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Download DataSource: " + respense);
        System.out.println("START Update DataSource ###");
        try {
            // now publish the workbook
            String project_id = tab1.tsGetProjectIdByName("Tableau Samples");
            String owner_id = tab1.tsGetUserIdByName(ts_user);
            respense = tab1.tsUpdateDataSource(datasource_id, "Wes Test DataSource 33",project_id,owner_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--DataSource Update failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Update DataSource: " + datasource_id);
        System.out.println("START Add Permission ###");
        try {
            // now publish the workbook
            String user_id = tab1.tsGetUserIdByName(ts_user);
            String capability_name = "ChangePermissions";
            String capability_mode = "Deny";
            String group_id = "";
            respense = tab1.tsAddDataSourcePermission(datasource_id, user_id, group_id, capability_name, capability_mode);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Add Permission: " + datasource_id);
        try {
            Thread.sleep(3000);                 //1000 milliseconds is one second.
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println("START Check DataSources ###");
        try {
            users = tab1.tsGetDataSources();
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = users.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(users[mycnt]!=null){
                // parse the separate fields
                String msite_id = users[mycnt].split(",")[0];
                String site_name = users[mycnt].split(",")[1];

                // display the results
                System.out.println("DataSourceName: " + site_name);
                try {
                    System.out.println("toID: " + tab1.tsGetDataSourceIdByName(site_name));
                    System.out.println("toName: " + tab1.tsGetDataSourceNameById(msite_id));
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }

                ////////////////////////////////////////
                // Permissions
                ////////////////////////////////////////
                System.out.println("Permissions:");
                String[] views = null;

                // here we will explore further and get all of the views for the workbook
                try {
                    views = tab1.tsGetPermissionsFromDataSource(msite_id);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // handle the response
                int mycnt2 = 0;
                int item_cnt2 = views.length;
                while(mycnt2<item_cnt2){
                    if(views[mycnt2]!=null){
                        System.out.println(views[mycnt2]);
                        String v_id = views[mycnt2].split(",")[0];
                        String v_name = views[mycnt2].split(",")[1];
                        //String v_user_id = views[mycnt2].split(",")[2];

                        // display the results
                        System.out.println("    "+v_name);
                    }
                    mycnt2++;
                }
                ////////////////////////////////////////
                // END of Permissions
                ////////////////////////////////////////
                System.out.println("---");

                try {
                    String resp = tab1.tsUpdateDataSourceConnection(msite_id,"test.com","8585","testu","testp");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    //e.printStackTrace();
                }
            }
            mycnt++;
        }
        System.out.println("DONE Check DataSources: ");
        System.out.println("START Delete Permission ###");
        try {
            // now publish the workbook
            String user_id = tab1.tsGetUserIdByName(ts_user);
            String capability_name = "ChangePermissions";
            String capability_mode = "Deny";
            String group_id = "";
            tab1.tsDeleteDataSourcePermission(datasource_id, user_id, group_id, capability_name, capability_mode);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Delete Permission: " + datasource_id);
        System.out.println("START Delete DataSource ###");
        try {
            // now publish the workbook
            tab1.tsDeleteDataSource(datasource_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--DataSource Delete Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Delete DataSource: ");
    }

    public static void testProjects() {
        String[] users = null;
        String respense = "";

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        String project_id = "";

        System.out.println("START Add Project ###");
        try {
            // now publish the workbook
            project_id = tab1.tsAddProject("Wes Test Project 1","Proj1 Desc");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Project Already Exists");
            //e.printStackTrace();
        }
        if(project_id.length()< 1)
        {
            try {
                project_id = tab1.tsGetProjectIdByName("Wes Test Project 1");
            } catch (Exception e2) {
                //
                System.out.println("--Something is wrong");
                project_id = "";
            }
        }
        System.out.println("DONE Add Project: " + project_id);
        System.out.println("START Update Project ###");
        try {
            // now publish the workbook
            respense = tab1.tsUpdateProject(project_id, "Wes Test Project 33","");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Project Update failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Update Project: " + project_id);
        System.out.println("START Add Permission ###");
        try {
            // now publish the workbook
            String user_id = tab1.tsGetUserIdByName(ts_user);
            String capability_name = "ViewComments";
            String capability_mode = "Deny";
            String group_id = "";

            respense = tab1.tsAddProjectPermission(project_id, user_id, group_id, capability_name, capability_mode);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Add Permission: " + project_id);
        try {
            Thread.sleep(3000);                 //1000 milliseconds is one second.
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println("START Check Projects ###");
        try {
            users = tab1.tsGetProjects();
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = users.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(users[mycnt]!=null){
                // parse the separate fields
                String msite_id = users[mycnt].split(",")[0];
                String site_name = users[mycnt].split(",")[1];

                // display the results
                System.out.println("Name: " + site_name);
                try {
                    System.out.println("toID: " + tab1.tsGetProjectIdByName(site_name));
                    System.out.println("toName: " + tab1.tsGetProjectNameById(msite_id));
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }
                ////////////////////////////////////////
                // Permissions
                ////////////////////////////////////////
                System.out.println("Permissions:");
                String[] views = null;

                // here we will explore further and get all of the views for the workbook
                try {
                    views = tab1.tsGetPermissionsFromProject(msite_id);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // handle the response
                int mycnt2 = 0;
                int item_cnt2 = views.length;
                while(mycnt2<item_cnt2){
                    if(views[mycnt2]!=null){
                        System.out.println(views[mycnt2]);
                        String v_id = views[mycnt2].split(",")[0];
                        String v_name = views[mycnt2].split(",")[1];
                        //String v_user_id = views[mycnt2].split(",")[2];

                        // display the results
                        System.out.println("    "+v_name);
                    }
                    mycnt2++;
                }
                ////////////////////////////////////////
                // END of Permissions
                ////////////////////////////////////////

                System.out.println("---");
            }
            mycnt++;
        }
        System.out.println("DONE Check Projects: ");
        System.out.println("START Delete Permission ###");
        try {
            // now publish the workbook
            String user_id = tab1.tsGetUserIdByName(ts_user);
            String capability_name = "ViewComments";
            String capability_mode = "Deny";
            String group_id = "";

            tab1.tsDeleteProjectPermission(project_id, user_id, group_id, capability_name, capability_mode);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Delete Permission: " + project_id);
        System.out.println("START Delete Project ###");
        try {
            // now publish the workbook
            tab1.tsDeleteProject(project_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Project Delete Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Delete Project: ");

    }

    public static void testUsersGroups() {
//		String users = "";
        String respense = "";

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        String group_id = "";

        System.out.println("START Add Group ###");
        try {
            // now publish the workbook
            group_id = tab1.tsAddGroup("Wes Test Group 1");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Group Already Exists");
            try {
                group_id = tab1.tsGetGroupIdByName("Wes Test Group 1");
            } catch (Exception e2) {
                //
            }
            //e.printStackTrace();
        }
        System.out.println("DONE Add Group: " + group_id);

        System.out.println("START Add User ###");
        String user_id = "";
        try {
            // now publish the workbook
            user_id = tab1.tsAddUser("newuser1","New User 1", "newuser1@test.com", "p@ssword", "Publisher");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--User Already Exists");
            //e.printStackTrace();
        }
        System.out.println("DONE Add User: " + user_id);
        System.out.println("START Add User To Group ###"+group_id);
        try {
            // now publish the workbook
            respense = tab1.tsAddUserToGroup(group_id, user_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--User Already Exists In Group");
            //e.printStackTrace();
        }
        System.out.println("DONE Add User To Group: " + respense);
        System.out.println("START Update Group ###");
        try {
            // now publish the workbook
            respense = tab1.tsUpdateGroup(group_id, "Group Wes 2");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Group Update Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Update Group: " + respense);
        System.out.println("START List Groups/Users");
        String[] users = new String[20];

        // try the function here to get the users workbooks
        try {
            System.out.println("   GroupName: " + tab1.tsGetGroupNameById(group_id));
            users = tab1.tsGetUsersFromGroup(group_id);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = users.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(users[mycnt]!=null){
                // parse the separate fields
                String site_id = users[mycnt].split(",")[0];
                String site_name = users[mycnt].split(",")[1];

                // display the results
                String uid = "";
                System.out.println("MainUser: " + site_name);
                try {
                    uid = tab1.tsGetUserIdByName(site_name);
                    System.out.println("   ToID: " + uid);
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }
                try {
                    String uid2 = tab1.tsGetUserNameById(uid);
                    System.out.println("   BackToName: " + uid2);
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }
                try {
                    String[] uid2 = tab1.tsGetUserById(uid);
                    System.out.println("   BackToName2: " + uid2[0].split(",")[1].toString());
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }

            }
            mycnt++;
        }

        System.out.println("DONE List Groups/Users");
        try {
            user_id = tab1.tsGetUserIdByName("newuser1");
        } catch (Exception e) {
            // catch errors and handle
        }

/*
 * API ERROR
 *
		System.out.println("START Delete Group ###");

		try {
			// now publish the workbook
			String group_id = tab1.tsGetGroupIdByName("Wes Test Group 1");
			tab1.tsDeleteGroup(group_id);
		} catch (Exception e) {
			// catch errors and handle
			System.out.println("--Group Delete Failed");
			//e.printStackTrace();
		}

		System.out.println("DONE Delete Group: " + respense);
*/
        System.out.println("START Delete GroupUser ###");
        try {
            // now publish the workbook
            group_id = tab1.tsGetGroupIdByName("Wes Test Group 1");
            tab1.tsDeleteUserFromGroup(user_id, group_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Group Delete Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Delete Group: " + respense);
        System.out.println("START Delete User ###");
        try {
            // now publish the workbook
            tab1.tsDeleteUserFromSite(user_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--User Delete Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Delete User: " + respense);

    }

    public static void testWorkbooks(String workbook_location, String workbook_name, String project_name, String save_location, String favorite_name, String tag_name) {
        String users = "";
        String respense = "";
        String group_id = "";
        String[] usersl = new String[200];
        String[] views = new String[200];
        String[] conns = new String[200];

        System.out.println("START Publish ###");
        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            users = tab1.tsPublishWorkbook(workbook_location, workbook_name, project_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Publish: " + users);
        System.out.println("START Download ###");
        try {
            // now publish the workbook
            users = tab1.tsDownloadWorkbook(workbook_name, save_location);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Download: " + users);
        System.out.println("START Update Workbook ###");
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String project_id = tab1.tsGetProjectIdByName("Tableau Samples");
            String owner_id = tab1.tsGetUserIdByName(ts_user);
            respense = tab1.tsUpdateWorkbook(workbook_id, project_id, "YES", owner_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Workbook Update Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Update Workbook: " + group_id);
        System.out.println("START Add Favorite ###");
        try {
            // now publish the workbook
            String user_id = tab1.tsGetUserIdByName(ts_user);
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            respense = tab1.tsAddWorkbookFavorite(workbook_id, user_id, favorite_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Add Favorite: " + group_id);
        System.out.println("START Add Tag ###");
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            respense = tab1.tsAddWorkbookTag(workbook_id, tag_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Add Tag: " + group_id);
        System.out.println("START Add Permission ###");
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String user_id = tab1.tsGetUserIdByName(ts_user);
            String capability_name = "ViewComments";
            String capability_mode = "Deny";
            respense = tab1.tsAddWorkbookPermission(workbook_id, user_id, group_id, capability_name, capability_mode);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Add Permission: " + group_id);
        System.out.println("START List Workbooks ###");
        try {
            usersl = tab1.tsGetWorkbooksByUser(ts_user);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = usersl.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(usersl[mycnt]!=null){
                // parse the separate fields
                String wb_id = usersl[mycnt].split(",")[0];
                String wb_name = usersl[mycnt].split(",")[1];

                // display the results
                System.out.println("Workbook: " + wb_name);

                ////////////////////////////////////////
                // Views
                ////////////////////////////////////////
                System.out.println("Views:");
                // here we will explore further and get all of the views for the workbook
                try {
                    views = tab1.tsGetViewsFromWorkbook(wb_id);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // handle the response
                int mycnt2 = 0;
                int item_cnt2 = views.length;
                while(mycnt2<item_cnt2){
                    if(views[mycnt2]!=null){
                        String v_id = views[mycnt2].split(",")[0];
                        String v_name = views[mycnt2].split(",")[1];
                        // display the results
                        System.out.println("    "+v_name);
                    }
                    mycnt2++;
                }
                ////////////////////////////////////////
                // END of Views
                ////////////////////////////////////////

                ////////////////////////////////////////
                // Connections
                ////////////////////////////////////////
                System.out.println("Connections:");
                // here we will explore further and get all of the views for the workbook
                try {
                    conns = tab1.tsGetWorkbookConnections(wb_id);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // handle the response
                int mycnt3 = 0;
                int item_cnt3 = conns.length;
                while(mycnt3<item_cnt3){
                    if(conns[mycnt3]!=null){
                        String c_id = conns[mycnt3].split(",")[0];
                        String c_type = conns[mycnt3].split(",")[1];
                        String c_name = "";

                        try {
                            String resp = tab1.tsUpdateWorkbookConnection(wb_id,c_id,"test.com","8585","testu","testp");
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            //e.printStackTrace();
                        }

                        // display the results
                        System.out.println("    "+c_name+"["+c_type+"]("+c_id+")");
                    }
                    mycnt3++;
                }
                ////////////////////////////////////////
                // END of Connections
                ////////////////////////////////////////
                ////////////////////////////////////////
                // Permissions
                ////////////////////////////////////////
                System.out.println("Permissions:");
                views = null;

                // here we will explore further and get all of the views for the workbook
                try {
                    views = tab1.tsGetPermissionsFromWorkbook(wb_id);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // handle the response
                mycnt2 = 0;
                item_cnt2 = views.length;
                while(mycnt2<item_cnt2){
                    if(views[mycnt2]!=null){
                        System.out.println(views[mycnt2]);
                        String v_id = views[mycnt2].split(",")[0];
                        String v_name = views[mycnt2].split(",")[1];
                        //String v_user_id = views[mycnt2].split(",")[2];

                        // display the results
                        System.out.println("    "+v_name);
                    }
                    mycnt2++;
                }
                ////////////////////////////////////////
                // END of Permissions
                ////////////////////////////////////////
                ////////////////////////////////////////
                // Tags
                ////////////////////////////////////////
                System.out.println("Tags:");
                views = null;

                // here we will explore further and get all of the views for the workbook
                try {
                    views = tab1.tsGetWorkbookTags(wb_id);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // handle the response
                mycnt2 = 0;
                item_cnt2 = views.length;
                while(mycnt2<item_cnt2){
                    if(views[mycnt2]!=null){
                        //System.out.println(views[mycnt2]);
                        String v_id = views[mycnt2].split(",")[0];
                        //String v_user_id = views[mycnt2].split(",")[2];

                        // display the results
                        System.out.println("    "+v_id);
                    }
                    mycnt2++;
                }
                ////////////////////////////////////////
                // END of Tags
                ////////////////////////////////////////


            }
            mycnt++;
        }
        System.out.println("DONE List Workbooks ###");
        System.out.println("START Delete Favorite ###");
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String user_id = tab1.tsGetUserIdByName(ts_user);
            tab1.tsDeleteWorkbookFavorite(workbook_id, user_id);
        } catch (Exception e) {
            // catch errors and handle
            //e.printStackTrace();
        }
        System.out.println("DONE Delete Favorite: " + group_id);
        System.out.println("START Delete Tag ###");
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            tab1.tsDeleteWorkbookTag(workbook_id, tag_name);
        } catch (Exception e) {
            // catch errors and handle
            //e.printStackTrace();
        }
        System.out.println("DONE Delete Tag: " + group_id);
        System.out.println("START Delete Permission ###");
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String user_id = tab1.tsGetUserIdByName(ts_user);
            String capability_name = "ViewComments";
            String capability_mode = "Deny";
            tab1.tsDeleteWorkbookPermission(workbook_id, user_id, group_id, capability_name, capability_mode);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Delete Permission: " + group_id);
        System.out.println("START Delete Workbook ###");
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            tab1.tsDeleteWorkbook(workbook_id);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }
        System.out.println("DONE Delete Workbook: " + users);
    }

    public static void testUpdateWorkbook(String workbook_name, String project_name) {
        String users = "";
        String respense = "";


        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        String group_id = "";

        System.out.println("START Update Workbook ###");
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String project_id = tab1.tsGetProjectIdByName("Tableau Samples");
            String owner_id = tab1.tsGetUserIdByName(ts_user);
            group_id = tab1.tsUpdateWorkbook(workbook_id, project_id, "YES", owner_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Workbook Update Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Update Workbook: " + group_id);

    }

    public static void testAddGroupsUsers() {
        String users = "";
        String respense = "";

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        String group_id = "";

        System.out.println("START Add Group ###");
        try {
            // now publish the workbook
            group_id = tab1.tsAddGroup("Wes Test Group 1");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Group Already Exists");
            //e.printStackTrace();
        }
        System.out.println("DONE Add Group: " + group_id);

        System.out.println("START Add User ###");
        String user_id = "";
        try {
            // now publish the workbook
            user_id = tab1.tsAddUser("newuser1","New User 1", "newuser1@test.com", "p@ssword", "Publisher");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--User Already Exists");
            //e.printStackTrace();
        }
        System.out.println("DONE Add User: " + user_id);
        System.out.println("START Add User To Group ###");
        try {
            // now publish the workbook
            respense = tab1.tsAddUserToGroup(group_id, user_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--User Already Exists In Group");
            //e.printStackTrace();
        }
        System.out.println("DONE Add User To Group: " + respense);
        System.out.println("START Update Group ###");
        try {
            // now publish the workbook
            respense = tab1.tsUpdateGroup(group_id, "Group Wes 2");
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Group Update Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Update Group: " + respense);

    }

    public static void testDeleteGroupsUsers() {
        String users = "";
        String respense = "";
        String user_id = "";
        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);

        try {
            user_id = tab1.tsGetUserIdByName("newuser1");
        } catch (Exception e) {
            // catch errors and handle
        }

		/*
		System.out.println("START Delete Group ###");

		try {
			// now publish the workbook
			String group_id = tab1.tsGetGroupIdByName("Wes Test Group 1");
			tab1.tsDeleteGroup(group_id);
		} catch (Exception e) {
			// catch errors and handle
			System.out.println("--Group Delete Failed");
			//e.printStackTrace();
		}

		System.out.println("DONE Delete Group: " + respense);
*/
        System.out.println("START Delete GroupUser ###");
        try {
            // now publish the workbook
            String group_id = tab1.tsGetGroupIdByName("Wes Test Group 1");
            tab1.tsDeleteUserFromGroup(user_id, group_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--Group Delete Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Delete Group: " + respense);
        System.out.println("START Delete User ###");
        try {
            // now publish the workbook
            tab1.tsDeleteUserFromSite(user_id);
        } catch (Exception e) {
            // catch errors and handle
            System.out.println("--User Delete Failed");
            //e.printStackTrace();
        }
        System.out.println("DONE Delete User: " + respense);
    }

    public static void testDeleteViewFav(String workbook_name, String view_name) {
        String users = "";

        System.out.println("START Delete View Favorite ###");

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String view_id = tab1.tsGetViewIdByName(workbook_id, view_name);
            String user_id = tab1.tsGetUserIdByName(ts_user);
            tab1.tsDeleteViewFavorite(view_id, user_id);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        System.out.println("DONE Delete View Favorite: " + users);
    }

    public static void testDeleteWorkbookTag(String workbook_name, String tag_name) {
        String users = "";

        System.out.println("START Delete Workbook Tag ###");

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            tab1.tsDeleteWorkbookTag(workbook_id, tag_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        System.out.println("DONE Delete Workbook Tag: " + users);
    }

    public static void testDeleteWorkbookFav(String workbook_name) {
        String users = "";

        System.out.println("START Delete Workbook Favorite ###");

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String user_id = tab1.tsGetUserIdByName(ts_user);
            tab1.tsDeleteWorkbookFavorite(workbook_id, user_id);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        System.out.println("DONE Delete Workbook Favorite: " + users);
    }

    public static void testAddViewFav(String workbook_name, String view_name, String fav_name) {
        String users = "";

        System.out.println("START Add ViewFavorite ###");

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            String user_id = tab1.tsGetUserIdByName(ts_user);
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String view_id = tab1.tsGetViewIdByName(workbook_id, view_name);
            String respense = tab1.tsAddViewFavorite(view_id, user_id, fav_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        System.out.println("DONE Add ViewFavorite: ");
    }

    public static void testWorkbookAddTag(String workbook_name, String tag_name) {
        String users = "";

        System.out.println("START Add Tag ###");

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String respense = tab1.tsAddWorkbookTag(workbook_id, tag_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        System.out.println("DONE Add Tag: " + users);
    }

    public static void testWorkbookAddFav(String workbook_name, String favorite_name) {
        String users = "";

        System.out.println("START Add Favorite ###");

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            String user_id = tab1.tsGetUserIdByName(ts_user);
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            String respense = tab1.tsAddWorkbookFavorite(workbook_id, user_id, favorite_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        System.out.println("DONE Add Favorite: " + users);
    }

    public static void testDeleteWorkbook(String workbook_name) {
        String users = "";

        System.out.println("START Delete Workbook ###");

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            String workbook_id = tab1.tsGetWorkbookIdByName(workbook_name);
            tab1.tsDeleteWorkbook(workbook_id);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        System.out.println("DONE Delete Workbook: " + users);
    }

    public static void testDownloadWorkbook(String workbook_name, String save_location) {
        String users = "";

        System.out.println("START Download ###");

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            users = tab1.tsDownloadWorkbook(workbook_name, save_location);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        System.out.println("DONE Download: " + users);
    }

    public static void testPublishWorkbook(String workbook_location, String workbook_name, String project_name) {
        String users = "";

        System.out.println("START Publish ###");

        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            // now publish the workbook
            users = tab1.tsPublishWorkbook(workbook_location, workbook_name, project_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        System.out.println("DONE Publish: " + users);

    }

    public static void testGetWorkbookPermissions() {
        // STEP #2: here you set your tableau server url
        String[] users = new String[20];
        String[] views = new String[20];
        String[] conns = new String[20];

        // try the function here to get the users workbooks
        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);
        try {
            users = tab1.tsGetWorkbooksByUser(user_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = users.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(users[mycnt]!=null){
                // parse the separate fields
                String wb_id = users[mycnt].split(",")[0];
                String wb_name = users[mycnt].split(",")[1];

                // display the results
                System.out.println("#################################");
                System.out.println("Workbook: " + wb_name);

                ////////////////////////////////////////
                // Views
                ////////////////////////////////////////
                System.out.println("Permissions:");
                // here we will explore further and get all of the views for the workbook
                try {
                    views = tab1.tsGetPermissionsFromWorkbook(wb_id);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // handle the response
                int mycnt2 = 0;
                int item_cnt2 = users.length;
                while(mycnt2<item_cnt2){
                    if(views[mycnt2]!=null){
                        System.out.println(views[mycnt2]);
                        String v_id = views[mycnt2].split(",")[0];
                        String v_name = views[mycnt2].split(",")[1];
                        //String v_user_id = views[mycnt2].split(",")[2];

                        // display the results
                        //System.out.println(v_id+"    "+v_name);
                    }
                    mycnt2++;
                }
                ////////////////////////////////////////
                // END of Views
                ////////////////////////////////////////

            }
            mycnt++;
        }
    }

    public static void testGetGroupsFromSite() {
        String[] users = new String[20];

        // try the function here to get the users workbooks
        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);

        try {
            users = tab1.tsGetGroupsFromSite();
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = users.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(users[mycnt]!=null){
                // parse the separate fields
                String site_id = users[mycnt].split(",")[0];
                String site_name = users[mycnt].split(",")[1];

                // display the results
                String uid = "";
                System.out.println("MainGroup: " + site_name);
                try {
                    uid = tab1.tsGetGroupIdByName(site_name);
                    System.out.println("   ToID: " + uid);
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }
                try {
                    String uid2 = tab1.tsGetGroupNameById(uid);
                    System.out.println("   BackToName: " + uid2);
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }

            }
            mycnt++;
        }
    }

    public static void testGetUsersFromSite() {
        String[] users = new String[20];

        // try the function here to get the users workbooks
        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);

        try {
            users = tab1.tsGetUsersFromSite();
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = users.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(users[mycnt]!=null){
                // parse the separate fields
                String site_id = users[mycnt].split(",")[0];
                String site_name = users[mycnt].split(",")[1];

                // display the results
                String uid = "";
                System.out.println("MainUser: " + site_name);
                try {
                    uid = tab1.tsGetUserIdByName(site_name);
                    System.out.println("   ToID: " + uid);
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }
                try {
                    String uid2 = tab1.tsGetUserNameById(uid);
                    System.out.println("   BackToName: " + uid2);
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }
                try {
                    String[] uid2 = tab1.tsGetUserById(uid);
                    System.out.println("   BackToName2: " + uid2[0].split(",")[1].toString());
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }

            }
            mycnt++;
        }
    }

    public static void testGetUsersFromGroup() {
        String[] users = new String[20];

        // try the function here to get the users workbooks
        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);

        try {
            String group_id = tab1.tsGetGroupIdByName("Wes Test Group 1");
            users = tab1.tsGetUsersFromGroup(group_id);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = users.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(users[mycnt]!=null){
                // parse the separate fields
                String site_id = users[mycnt].split(",")[0];
                String site_name = users[mycnt].split(",")[1];

                // display the results
                String uid = "";
                System.out.println("MainUser: " + site_name);
                try {
                    uid = tab1.tsGetUserIdByName(site_name);
                    System.out.println("   ToID: " + uid);
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }
                try {
                    String uid2 = tab1.tsGetUserNameById(uid);
                    System.out.println("   BackToName: " + uid2);
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }
                try {
                    String[] uid2 = tab1.tsGetUserById(uid);
                    System.out.println("   BackToName2: " + uid2[0].split(",")[1].toString());
                } catch (Exception e) {
                    // catch errors and handle
                    e.printStackTrace();
                }

            }
            mycnt++;
        }
    }

    public static void testGetAllWorkbooks() {
        // STEP #2: here you set your tableau server url
        String[] users = new String[20];
        String[] views = new String[20];
        String[] conns = new String[20];

        // try the function here to get the users workbooks
        // create a handle for a Tableau Server
        tabRest tab1 = new tabRest(ts_url, ts_user, ts_pass, ts_site);

        try {
            users = tab1.tsGetWorkbooksByUser(user_name);
        } catch (Exception e) {
            // catch errors and handle
            e.printStackTrace();
        }

        // handle the response
        int mycnt = 0;
        // determine the number of records
        int item_cnt = users.length;

        // process the records
        while(mycnt<item_cnt){
            // if this is not a null record
            if(users[mycnt]!=null){
                // parse the separate fields
                String wb_id = users[mycnt].split(",")[0];
                String wb_name = users[mycnt].split(",")[1];

                // display the results
                System.out.println("#################################");
                System.out.println("Workbook: " + wb_name);

                ////////////////////////////////////////
                // Views
                ////////////////////////////////////////
                System.out.println("Views:");
                // here we will explore further and get all of the views for the workbook
                try {
                    views = tab1.tsGetViewsFromWorkbook(wb_id);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // handle the response
                int mycnt2 = 0;
                int item_cnt2 = users.length;
                while(mycnt2<item_cnt2){
                    if(views[mycnt2]!=null){
                        String v_id = views[mycnt2].split(",")[0];
                        String v_name = views[mycnt2].split(",")[1];
                        // display the results
                        System.out.println("    "+v_name);
                    }
                    mycnt2++;
                }
                ////////////////////////////////////////
                // END of Views
                ////////////////////////////////////////

                ////////////////////////////////////////
                // Connections
                ////////////////////////////////////////
                System.out.println("Connections:");
                // here we will explore further and get all of the views for the workbook
                try {
                    conns = tab1.tsGetWorkbookConnections(wb_id);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // handle the response
                int mycnt3 = 0;
                int item_cnt3 = conns.length;
                while(mycnt3<item_cnt3){
                    if(conns[mycnt3]!=null){
                        String c_id = conns[mycnt3].split(",")[0];
                        String c_type = conns[mycnt3].split(",")[1];
                        String c_name = "";

                        // display the results
                        System.out.println("    "+c_name+"["+c_type+"]("+c_id+")");
                    }
                    mycnt3++;
                }
                ////////////////////////////////////////
                // END of Connections
                ////////////////////////////////////////
            }
            mycnt++;
        }
    }

}
