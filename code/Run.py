# import libraries and classes
import Constants
import DataMine

class Run:
    if __name__ == "__main__":

        const = Constants.Constants()
        dm = DataMine.DataMine()

        mortality_total, mortality_male, mortality_female =dm.mine_data_from_excel()

