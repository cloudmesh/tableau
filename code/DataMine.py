# this class extracts data from the CSV files
# this class extracts the mortality total from the excel files with a condition
# the condition is to retrieve data with mortality rate not null and mortality rate lesser than or
# equal to 2500
# import excel python library
import openpyxl
import pandas as pd
import os.path
import json
import requests
import Constants


class DataMine:

    url = Constants.Constants().data_source_url
    # initialization method
    def init(self, method):
        print("Initializing " + method + " ...")

    def get_path_for_file(self):
        self.init("Get CSV File Path")
        url = self.url
        return url

    def mine_data_from_excel(self):
        # this columnn holds the specific data set that is needed in the program
        state = "mort-total-US04"
        self.init("Mine data from CSV files")
        mortality_rate = 0.0
        self.init('Read Power Data')
        filepath = self.get_path_for_file()
        print(filepath)
        df = pd.ExcelFile(filepath).parse('Sheet1')  # reads file
        #print(df.columns)
        mortality_total=[]
        mortality_male=[]
        mortality_female = []
        column = df['mort-total-US04']
        length = len(column)
        for i in range(0, length):
            val = column[i]
            if(val >= 0 and val <= 2500):
                print(val)
                mortality_total.append(val)

        column = df['mort-male-US04']
        length = len(column)
        for i in range(0, length):
            val = column[i]
            if (val >= 0 and val <= 2500):
                print(val)
                mortality_male.append(val)

        column = df['mort-female-US04']
        length = len(column)
        for i in range(0, length):
            val = column[i]
            if (val >= 0 and val <= 2500):
                print(val)
                mortality_female.append(val)

        return mortality_total, mortality_male, mortality_female

    # represents the column numbers
    def get_range(self):
        start = 17
        end = 19

        return start, end






