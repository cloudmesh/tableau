================================
  Tableau 
================================

--------------------------------
 Bonus Project
--------------------------------

:Author: Sushmita Ray; Vibhatha Abeykoon
:Contact: sushray@iu.edu; vlabeyko@umail.iu.edu
:organization: Indiana University Bloomington
:status: Completed

:abstract:

    This project describes Tableau with some examples like tutorial or video. 
    It lists various Tableau products which are used extensively in the market 
    and provides their uses and limitations. To have a better understanding this 
    project will perform a data visualization using Tableau Desktop using a free 
    online dataset. To have a better understanding, this project will
    perform a data visualization using Tableau Desktop using a
    free online dataset. The additional features like Javascript
    orchestration with Tableau is also explained with some ex-
    amples. In this project, the installation of Tableau SDK
    python version on Linux distributions will also be discussed.


.. contents:: Table of Contents
.. section-numbering::

Data Visualization 
===================

Data visualization is the process of describing information through visual 
rendering. Patterns, trends and correlations that might go undetected in 
text-based data can be exposed and recognized easier with data visualization 
software. Businesses around the globe realize that the ability to visualize 
data effectively leads directly to better understanding, insight and better 
business decisions. Today's data visualization tools go beyond the standard 
charts and graphs used in Excel spreadsheets, displaying data in more 
sophisticated ways such as infographics, dials and gauges, geographic maps, 
heat maps, and detailed bar, and pie charts. Tableau Software, is one of them, 
that enables businesses to keep pace with the evolving technology landscape 
and outperform competitors through an adaptive and intuitive means of 
visualizing their data.

What is Tableau 
=================
Tableau is a data visualization software that connects easily to nearly any 
data source, be it corporate Data Warehouse, Microsoft Excel or web-based data. 
It allows for instantaneous insight by transforming data into visually appealing, 
interactive visualizations called dashboards. This process takes only seconds or 
minutes rather than months or years, and is achieved through the use of an easy 
to use drag-and-drop interface.

Tableau Products 
===================
Tableau desktop 
-------------------

It is a design is based on technology that lets you drag & drop to analyze data. 
One can connect to data in a few clicks, then visualize and create interactive 
dashboards. It leverages natural ability to spot visual patterns quickly and easy 
to use. It is compatible with any data, be a spreadsheet, a SQL database, Hadoop, 
or the cloud. It helps to perform real analytics by building new calculations from 
existing data, drag in reference lines and forecasts, and access statistical 
summaries. It also  experiments with trend analyses, regressions and correlations.

Tableau Reader
------------------------
It lets us read files saved in Tableau Desktop.

Tableau server 
--------------------------------------------
Tableau Server is a business intelligence application that provides browser-based 
analytics anyone can use. 

* **Shareable**. Tableau Server users can create workbooks and views, dashboards, 
  and data sources in Tableau Desktop, and then publish this content to the server.

* **Secure**. Tableau Server site and server administrators control who has access 
  to server content to help protect sensitive data. Administrators can set user 
  permissions on projects, workbooks, views, and data sources.

* **Mobile**. Users can see and interact with the most up-to-date server content 
  from anywhere, whether they use a browser or mobile device.

Tableau online (Cloud analytics for organizations)
----------------------------------------------------
Tableau Online is a hosted version of Tableau Server which provides live, 
interactive views of data that let people answer their own questions, right 
in a web browser or on a tablet in a secure, hosted environment. It can scale 
up as much needed. There is no need to purchase, set up or manage any infrastructure. 

Tableau public 
---------------
Tableau Public is for anyone who wants to tell stories with interactive data on the web. 
It's delivered as a service, up and running. With Tableau Public one can create interactive 
visuals and publish them quickly, without the help of programmers or IT. The Premium version 
of Tableau Public is for organizations that want to enhance their websites with interactive 
data visualizations. There are higher limits on the size of data to  be worked with. 
And among other premium features, data can be kept hidden.

Quick Features
===================
* Tableau Public and Tableau Reader are free to use, while both Tableau Server and Tableau Desktop come with a 14 days fully functional free trial period, after which the user must pay for the software.
* Tableau Desktop comes in both a Professional and a lower cost Personal edition. Tableau Online is available with an annual subscription for a single user, and scales to support thousands of users.
* Tableau also provides a development environment for developers to develop applications with dynamic data binding.

Tableau Tutorial
=================
Tableau provides Free Online, Live and Classroom (paid) training programs. To watch these training videos, first one needs to register. Then expand the “Getting Started” section and watch available three videos. These videos will talk about connecting with data, data preparation, building views, filters, dashboards, story points and ways to distribute.
`<https://www.tableau.com/learn/training>`_

Another resource to learn Tableau basics and its advanced processes is given below.
`<https://intellipaat.com/tutorial/tableau-tutorial/>`_
 


Visualization Examples (from Web)
=========================
* How Much Cash Does a Freshman Generate?. 
    `(Example 1) <https://public.tableau.com/en-us/s/gallery/freshman-cash-generation>`_
    
    .. image:: Net_Revenue_Data.png
* Global Hunger Index 2014: The International Food Policy Research Institute (IFPRI) has published the 2014 Global Hunger Index report and map.
    `(Example 2) <https://public.tableau.com/en-us/s/gallery/global-hunger-index-2014>`_
    
    .. image:: 2014_GHI.png
** (Refer to Tableau.pptx for details) **.

Visualization Example (self-created using Tableau Desktop) 
===========================================================
Dataset Used
-------------
The dataset presents U.S. Mortality Statistics for 2004, as classified by the World Health 
Organization's International Classification of Diseases 10 (ICD10). 

* About the International Classification of Diseases 10 (ICD10)* `(Links to an external site.)  
  <http://www.who.int/classifications/icd/en/>`_ This link provides background on the 
  classification system, and documentation for working with the system. The current version of 
  the ICD10 is available in tabular formats (provided) and CLaML XML format.
* Download the raw data files of the WHO Mortality Database* (`Links to an external site) 
  <http://www.who.int/healthinfo/statistics/mortality_rawdata/en/>`_
  This link provides documentation and access to a historic database provides that mortality 
  statistics for WHO, including countries outside of the U.S., broken out by the years that 
  statistics were collected for a given country.

Objective
----------
We have to choose the best information visualization technique(s) (e.g. statistical 
visualizations, tree maps) that provides some insights about what causes death in the 
United States based on the WHO ICD10 classification statistics. Also we will provide some 
comparison between genders in the final product, where appropriate.

Roadmap
----------
- **Steps for Tree Map US Mortality Statistics**. Sort the csv file based on "total mortality" in descending order and remove null rows. Import the csv file to Tableau public and select Tree Map layout with measures chapter, title and mort-total.
- **Steps for Gender Comparison**. Import the csv file to Tableau public and choose measures as chapter, mort-male and mort-female. Then select side-by-side bars for the visualization. 

Extraction and Visualization Process
--------------------------------------

- Data cleaning and Preprocessing carried out using Excel.
- Data Visualizations done using Tableau Desktop.

    **Steps for Tree Map US Mortality Statistics:**

        1.	Preprocessing: 
                   a. Sorted the csv file based on “total mortality” in descending order.
                   b. Removed the rows with mort- total=NULL && mort-total<=2500.
	               c. Kept the fields: chapter; block-3code; icd-code1; icd-code3; title; mort-total; mort-male; mort-female.	

        2.	Import the csv file **(US-Morbidity_final)** to Tableau public and select Tree Map layout with measures chapter, title and mort-total.


    **Steps for Gender Comparison:**

                    a. Import the csv file(US-Morbidity_final) to Tableau public and choose measures as chapter, mort-male and mort-female.
                    b. Then select side-by-side bars for the visualization.
                    c. Replaced the chapter numbers with their names from chapters.txt.
                    


Results and Interpretation
----------------------------

Nearly 75% of all deaths in the US are attributed to just 16 causes, with the top 3 of these accounting for over 55% of all deaths.
The WHO data (2004) reveals that there were 13,643,283 deaths registered in the US, which equates to:
    
    * 6,565,543 male
    * 7,077,740 female

Heart disease remains the leading cause of death in the US and affecting significantly more women than men.
Neoplasm and respiratory diseases are the next leading causes of death in order.

* **Comparison of gender mortality** `<https://public.tableau.com/views/Gendermortalitycomparison/Sheet1?:embed=y&:display_count=yes>`_

.. image:: Gender_Comparison.png

The death figures of female are about 1.2 times of male in total count. Females are more susceptible to heart, respiratory and Alzheimer disease.
Bronchus/lung/ bladder cancer killed male nearly twice than female. Causes like skin infections, congenital malfunction are the least contributing factors to both gender deaths.


* **Causes of deaths in the U.S** `<https://public.tableau.com/views/U_S_MortalityStatistics_0/Sheet1?:embed=y&:display_count=yes>`_

.. image:: US_Mortality_Causes.png


This shows the US Mortality statistics highlighting the primary causes of recorded death in the U.S. While diseases related to Circulatory system (Category 09) like heart disease is the leading cause of death,
there are some unclassified conditions (Category 18) which have remarkable contribution to the death rate. 

Visualization using Javascript and Tableau
--------------------------------------------
The impact from Tableau on interactive presentation of charts, graphs and analytics is admirable. And JavaScript plays a major role in this with a rich API documentation and examples.
The sample codes for the examples can be obtained at `<https://gitlab.com/cloudmesh/tableau/tree/master/rest/javascript>`_

The results can be found at the repository links:

    * `<https://gitlab.com/cloudmesh/tableau/blob/master/report/t0.png>`_
    * `<https://gitlab.com/cloudmesh/tableau/blob/master/report/t2.png>`_
    * `<https://gitlab.com/cloudmesh/tableau/blob/master/report/t3.png>`_
    * `<https://gitlab.com/cloudmesh/tableau/blob/master/report/t4.png>`_
    * `<https://gitlab.com/cloudmesh/tableau/blob/master/report/t5.png>`_